package com.todo.task;

import com.todo.task.teach.repository.StaffRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class TodotaskApplicationTests {

	@Autowired
	StaffRepository staffRepository;

	@Test
	void contextLoads() {
	}

	@Test
	void testDelete() {
		boolean result = staffRepository.deleteById(8);
		assertThat(result).isEqualTo(true);
	}
}
