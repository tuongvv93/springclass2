package com.todo.task.teach.repository.database;

import com.todo.task.teach.model.database.Staff;
import com.todo.task.teach.model.database.StaffRole;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface StaffDatabase extends JpaRepository<Staff, Integer>{

    @Query("select c from Staff c")
    public List<Staff> findAll2();

    @Query("select c from Staff c")
    public List<Staff> findWithPage(Pageable pageable);

    @Query(value = "select * from staff", nativeQuery = true)
    public List<Staff> findAllWithNativeQuery();

    @Query(value = "select * from staff limit ?1 offset ?2", nativeQuery = true)
    public List<Staff> findAllWithNativeQuery(int pageSize, int offset);

    @Query(value = "select * from staff", nativeQuery = true)
    public List<Staff> findAllWithNativeQuery2(Pageable pageable);


    @Query(value = "select full_name as fullName, role.name as roleName from staff, role where staff.role = role.type", nativeQuery = true)
    public List<StaffRole> findStaffInfo(Pageable pageable);

    @Modifying
    @Query("delete from Staff s where s.id = :id")
    public void deleteById(Integer id);
}