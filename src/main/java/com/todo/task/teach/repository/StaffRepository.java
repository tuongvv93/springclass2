package com.todo.task.teach.repository;

import com.todo.task.teach.model.database.Staff;
import com.todo.task.teach.model.database.StaffRole;
import com.todo.task.teach.repository.database.StaffDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StaffRepository {
    @Autowired
    StaffDatabase staffDatabase;

    @PersistenceContext
    EntityManager entityManager;

    public List<Staff> listAllStaff() {
        return staffDatabase.findAllWithNativeQuery();
    }

    public List<Staff> findWithPage(int pageIndex, int pageSize) {
        Pageable pageable = PageRequest.of(pageIndex, pageSize);
        return staffDatabase.findWithPage(pageable);
    }

    public List<Staff> findWithPageNativeQuery(int pageIndex, int pageSize) {
        return staffDatabase.findAllWithNativeQuery(pageSize, pageIndex * pageSize);
    }

    public List<Staff> findWithPageNativeQuery2(int pageIndex, int pageSize) {
        Pageable pageable = PageRequest.of(pageIndex, pageSize);
        return staffDatabase.findAllWithNativeQuery2(pageable);
    }

    public List<StaffRole> findStaffInfo(int pageIndex, int pageSize) {
        Pageable pageable = PageRequest.of(pageIndex, pageSize);
        return staffDatabase.findStaffInfo(pageable);
    }

    public boolean deleteById(int staffId) {
        staffDatabase.deleteById(staffId);
        return true;
    }

    public boolean saveStaff(Staff staff) {
//        staff.setCreatedAt(new Date());
//        staff.setUpdatedAt(new Date());
        Staff staffSaved = staffDatabase.save(staff);
        if (staffSaved != null) {
            return true;
        }
        return false;
    }

    public boolean saveStaff2(Staff staff) {
        EntityManager entity = entityManager.getEntityManagerFactory().createEntityManager();
        try {
            entity.getTransaction().begin();
            int result = entity.createNativeQuery("insert into Staff (name, user_name, password) value (:name,:user_name, :password)")
                    .setParameter("name", "wall vu")
                    .setParameter("user_name", "wall")
                    .setParameter("password", "1234")
                    .executeUpdate();
            if (result == 0) {
                return false;
            }
            if (result != 1) {
                return false;
            }
            List<Object[]> list = entity.createNativeQuery("select * from Staff")
                    .getResultList();
            List<Staff> staffList = new ArrayList<>();
            for (Object[] data : list) {
                staffList.add(Staff.builder()
                        .fullName((String) data[1])
                        .userName((String) data[2])
                        .password((String) data[3])
                        .build());
                throw new Exception();
            }

            entity.getTransaction().commit();
        } catch (Exception e) {
            entity.getTransaction().rollback();
        }
        return false;
    }
}
