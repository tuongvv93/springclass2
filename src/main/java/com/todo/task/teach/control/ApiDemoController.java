package com.todo.task.teach.control;

import com.todo.task.teach.model.base.BaseResponse;
import com.todo.task.teach.model.database.Staff;
import com.todo.task.teach.model.dto.ArrRequest;
import com.todo.task.teach.model.demo.Result;
import com.todo.task.teach.service.DemoService;
import com.todo.task.teach.service.StaffService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("teach")
public class ApiDemoController {
//    @Autowired
//    DemoService demoService;
    final DemoService demoService;
    final StaffService staffService;

    public ApiDemoController(DemoService demoService, StaffService staffService) {
        this.demoService = demoService;
        this.staffService = staffService;
    }

    @GetMapping("/helloworld")
    public String hello() {
        return "Hello World!";
    }

    @GetMapping("/helloworld-with-params")
    public String hello2(@RequestParam(required = false, value = "abc") String abc, @RequestParam(required = false) String xyz) {
        System.out.println("abc: " + abc);
        System.out.println("xyz: " + xyz);
        return "Hello World!";
    }

    @GetMapping("/helloworld-with-params/{language}")
    public String hello3(@PathVariable("language") String language) {
        System.out.println("language: " + language);
        if ("en".equals(language)) {
            return "Hello World!";
        }
        return "Xin chào!";
    }

    @PostMapping("/helloworld2")
    public ResponseEntity<BaseResponse> hello1() {
        //Student s = Student.builder().id(1).age(10).name("name").build();
        BaseResponse baseResponse = BaseResponse.builder().code(0).message("Đây là một mô tả lỗi cho người dùng").build();

        return ResponseEntity.ok(baseResponse);
    }

    @PostMapping("/post-demo")
    public ResponseEntity<BaseResponse<Result>> postDemo(@RequestBody ArrRequest arr) {
        Result result = demoService.findMaxAndMax2(arr.getA());
        BaseResponse<Result> baseResponse = new BaseResponse<>();
        baseResponse.setCode(1);
        baseResponse.setMessage("abc");
        baseResponse.setData(result);
        return ResponseEntity.ok(baseResponse);
    }
    // viết api bằng 2 phương thức get post đầu vào là 2 số nguyên lớn
    // => tính ra tích của nó và trả về người dùng



    // database demo
    @GetMapping("/getAllStaff")
    public List<Staff> getAllStaff() {
        return staffService.findAll();
    }

    @PostMapping("/saveStaff")
    public ResponseEntity<BaseResponse<Boolean>> saveStaff(@RequestBody Staff staff) {
        return ResponseEntity.ok(BaseResponse.<Boolean>builder()
                .code(1)
                .message("OK")
                .data(staffService.saveStaff(staff))
                .build());
    }

    @PostMapping("/saveStaffExcutor")
    public ResponseEntity<BaseResponse<Boolean>> saveStaffExcutor(@RequestBody Staff staff) {
        return ResponseEntity.ok(BaseResponse.<Boolean>builder()
                .code(1)
                .message("OK")
                .data(staffService.saveStaffExcutor(staff))
                .build());
    }

    @PostMapping("/saveStaff2")
    public ResponseEntity<BaseResponse<Boolean>> saveStaff2(@RequestBody Staff staff) {
        return ResponseEntity.ok(BaseResponse.<Boolean>builder()
                .code(1)
                .message("OK")
                .data(staffService.saveStaff2(staff))
                .build());
    }

}