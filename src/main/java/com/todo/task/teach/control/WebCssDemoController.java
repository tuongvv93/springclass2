package com.todo.task.teach.control;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("cms")
public class WebCssDemoController {

    @RequestMapping("/index")
    public String demoWeb(Model model) {
        return "cssdemo/index";
    }

}
