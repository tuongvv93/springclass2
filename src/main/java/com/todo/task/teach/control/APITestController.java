package com.todo.task.teach.control;

import com.todo.task.teach.model.database.Staff;
import com.todo.task.teach.model.database.StaffRole;
import com.todo.task.teach.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/teach2")
public class APITestController {
    @Autowired
    StaffService staffService;

    @GetMapping("/getWithPage")
    public List<Staff> getWithPage(@RequestParam Integer page, @RequestParam Integer pageSize) {
        return staffService.getWithPage(page, pageSize);
    }

    @GetMapping("/getWithPageNative")
    public List<Staff> getWithPageNative(@RequestParam Integer page, @RequestParam Integer pageSize) {
        return staffService.getWithPageNative(page, pageSize);
    }

    @GetMapping("/findStaffInfo")
    public List<StaffRole> findStaffInfo(@RequestParam Integer page, @RequestParam Integer pageSize) {
        return staffService.findStaffInfo(page, pageSize);
    }

}
