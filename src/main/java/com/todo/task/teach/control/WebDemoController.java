package com.todo.task.teach.control;

import com.todo.task.teach.model.demo.DataTest;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("teach")
public class WebDemoController {
    // localhost:8081/teach/webdemo
    @RequestMapping("/webdemo")
    public String demoWeb(Model model) {
        //model.addAttribute("hello", "Hello world!");
        return "demo/hello";
    }

    @PostMapping("webdemo3")
    public String demoWeb3(Model model, @RequestParam Map<String, String> body) {
        System.out.println(body.get("name") + " " + body.get("age"));
        model.addAttribute("name", "Name is: " + body.get("name"));
        model.addAttribute("age", "Age is: " + body.get("age"));
        return "demo/hello2";
    }

    @RequestMapping("/webdemo2")
    public String demoWeb2(Model model) {
        model.addAttribute("hello", "Hello world!");
        model.addAttribute("link", "https://google.com");

        List<DataTest> list = new ArrayList<>();
        list.add(DataTest.builder().name("Toan").build());
        list.add(DataTest.builder().name("Ly").build());
        list.add(DataTest.builder().name("Hoa").build());
        model.addAttribute("link", "https://google.com");
        model.addAttribute("list", list);

//        for (DataTest abc:list) {
//            System.out.println();
//        }
        return "demo/hello2";
    }
}
