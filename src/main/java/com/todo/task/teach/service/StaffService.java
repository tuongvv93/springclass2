package com.todo.task.teach.service;

import com.todo.task.teach.model.database.Staff;
import com.todo.task.teach.model.database.StaffRole;
import com.todo.task.teach.repository.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class StaffService {
    @Autowired
    StaffRepository staffRepository;
    @Autowired
    TaskExecutor taskExecutor;

    public List<Staff> findAll() {
        return staffRepository.listAllStaff();
    }

    public boolean saveStaff(Staff staff) {
        return staffRepository.saveStaff(staff);
    }

    public boolean saveStaffExcutor(Staff staff) {
        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                staffRepository.saveStaff(staff);
            }
        });
        return true;
    }

    public boolean saveStaff2(Staff staff) {
        return staffRepository.saveStaff2(staff);
    }

    public List<Staff> getWithPage(Integer page, Integer pageSize) {
        return staffRepository.findWithPage(page, pageSize);
    }

    public List<Staff> getWithPageNative(Integer page, Integer pageSize) {
        return staffRepository.findWithPageNativeQuery2(page, pageSize);
    }

    public List<StaffRole> findStaffInfo(Integer page, Integer pageSize) {
        return staffRepository.findStaffInfo(page, pageSize);
    }
}
