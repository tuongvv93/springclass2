package com.todo.task.teach.service;

import com.todo.task.teach.model.demo.Result;
import org.springframework.stereotype.Service;

@Service
public class DemoService {
    public Result findMaxAndMax2(int[] a) {
        int max1 = Integer.MIN_VALUE;
        int max2 = Integer.MIN_VALUE;
        for (int i = 0; i < a.length; i++) {
            if (max1 < a[i]) {
                max1 = a[i];
            }
            if (max2 < a[i] && a[i] < max1) {
                max2 = a[i];
            }
        }
        return Result.builder().max1(max1).max2(max2).build();
    }
}
