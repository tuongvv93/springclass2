package com.todo.task.teach.model.database;

public interface StaffRole {
    public String getFullName();
    public String getRoleName();
}
