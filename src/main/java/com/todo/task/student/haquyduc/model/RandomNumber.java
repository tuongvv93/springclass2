package com.todo.task.student.haquyduc.model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.Random;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RandomNumber {
    private int a;
    private int b;
}
