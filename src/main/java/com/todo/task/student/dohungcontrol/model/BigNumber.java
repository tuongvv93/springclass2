package com.todo.task.student.dohungcontrol.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BigNumber {
    private BigInteger a;
    private BigInteger b;
}
