package com.todo.task.student.dohungcontrol.control;

import com.todo.task.student.dohungcontrol.model.Animal;
import com.todo.task.student.dohungcontrol.model.database.Employee;
import com.todo.task.student.dohungcontrol.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
@RequestMapping("/webdohung")
public class WebControl {
    @Autowired
    EmployeeService employeeService;
    @GetMapping("number-random")
    public String numberRandom(@RequestParam int min, @RequestParam int max, Model model) {
        Random rd = new Random();
        String numberStr = String.valueOf(rd.nextInt((max - min) + 1) + min);
        model.addAttribute("number", numberStr);
        return "demo/webdohung";
    }

    @PostMapping("webdemohtml2")
    public String demoHTML2(Model model, @RequestParam Map<String, String> body) {
        System.out.println(body.get("name") + " " + body.get("age"));
        model.addAttribute("name", "name is:" + body.get("name"));
        model.addAttribute("age", "age is:" + body.get("age"));
        return "student/dohung/demohtml";
    }

    @RequestMapping("/webdemohtml")
    public String demoHTML(Model model) {
        List<Animal> list = new ArrayList<>();
        list.add(Animal.builder().name("Chim").build());
        list.add(Animal.builder().name("Su Tu").build());
        list.add(Animal.builder().name("Ran").build());
        model.addAttribute("list", list);
        return "student/dohung/demohtml";
    }

//    @PostMapping("baitap1")
//    public String webBai1(Model model, @RequestParam Map<String, String> body) {
//        float kq = Float.parseFloat(body.get("a")) + Float.parseFloat(body.get("b"));
//        model.addAttribute("kq","Kết quả của phép tổng là :"+ kq);
//        return "student/dohung/bai1";
//    }

    @RequestMapping("/baitap1")
    @PostMapping("/baitap1")
    public String baiTap1(Model model, @RequestParam Map<String, String> body) {
        if (body != null && body.size() > 0) {
            float kq = Float.parseFloat(body.get("a")) + Float.parseFloat(body.get("b"));
            model.addAttribute("kq", "Kết quả của phép tổng là :" + kq);
        }
        return "student/dohung/bai1";
    }

    @PostMapping("regexdemo")
    public String webRegex(Model model, @RequestParam Map<String, String> body) {
        System.out.println(body.get("input"));
        System.out.println(body.get("characters"));
        Pattern pattern = Pattern.compile(body.get("input"));
        Matcher matcher = pattern.matcher(body.get("characters"));
        model.addAttribute("ketqua",String.valueOf(matcher.find()));
        return "student/dohung/regex";
    }
    @RequestMapping("/webregex")
    public String regexDemo(Model model) {
        return "student/dohung/regex";
    }

    @RequestMapping("/apidemohtml")
    public String apiDemoHTML(Model model) {
        List<Employee> list = new ArrayList<>(employeeService.findAllEmployee());
        model.addAttribute("list1", list);
        return "student/dohung/apiwebdemo";
    }
}
