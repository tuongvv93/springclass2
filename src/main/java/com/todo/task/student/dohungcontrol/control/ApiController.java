package com.todo.task.student.dohungcontrol.control;

import com.todo.task.student.dohungcontrol.model.base.BaseResponse;
import com.todo.task.student.dohungcontrol.model.database.Employee;
import com.todo.task.student.dohungcontrol.model.database.EmployeeRole;
import com.todo.task.student.dohungcontrol.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/dohungapi")
public class ApiController {
    @Autowired
    EmployeeService employeeService;
    @GetMapping("/getAllEmployee")
    public List<Employee> getAllEmployee() {
        return employeeService.findAllEmployee();
    }
    @GetMapping("/getWithPage")
    public List<Employee> getWithPage(@RequestParam Integer page,@RequestParam Integer pageSize) {
        return employeeService.getWithPage(page,pageSize);
    }
    @PostMapping("/saveEmployee")
    public ResponseEntity<BaseResponse<Object>> saveEmployee(@RequestBody Employee employee) {
        return ResponseEntity.ok(BaseResponse.builder()
                .code(1)
                .message("OK")
                .data(employeeService.saveEmployee(employee))
                .build());

    }
    @GetMapping("/getInfo")
    public List<EmployeeRole> getInfo(@RequestParam Integer page,@RequestParam Integer pageSize) {
        return employeeService.getInfo(page,pageSize);
    }

}
