package com.todo.task.student.dohungcontrol.repository;

import com.todo.task.student.dohungcontrol.model.database.Students;
import com.todo.task.student.dohungcontrol.repository.database.StudentDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentRespository {
    @Autowired
    StudentDatabase studentDatabase;

    public List<Students> listAllStudent() {return studentDatabase.findAll(); }
}
