package com.todo.task.student.dohungcontrol.model.database;

public interface EmployeeRole {
    public String getName();
    public String getRoleName();
}
