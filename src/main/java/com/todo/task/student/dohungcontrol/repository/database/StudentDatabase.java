package com.todo.task.student.dohungcontrol.repository.database;

import com.todo.task.student.dohungcontrol.model.database.Students;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentDatabase extends JpaRepository<Students,Integer> {
}
