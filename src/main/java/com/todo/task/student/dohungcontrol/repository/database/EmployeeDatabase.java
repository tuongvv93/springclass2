package com.todo.task.student.dohungcontrol.repository.database;

import com.todo.task.student.dohungcontrol.model.database.Employee;
import com.todo.task.student.dohungcontrol.model.database.EmployeeRole;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeDatabase extends JpaRepository<Employee,Integer> {
    @Query(value = "select * from employee",nativeQuery = true)
    public List<Employee> findAllByNativeQuery();

    @Query("select c from Employee c")
    public List<Employee> findWithPage(Pageable pageable);

    @Query(value = "select employee.name as name,roles.rolename as roleName from employee , roles where employee.role = roles.type",nativeQuery = true)
    public List<EmployeeRole> findEmployeeInfo(Pageable pageable);
}
