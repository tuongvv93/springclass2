package com.todo.task.student.dohungcontrol.service;

import com.todo.task.student.dohungcontrol.model.BigNumber;
import com.todo.task.student.dohungcontrol.model.Result;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public class BigNumBerService {
    public Result findTich(BigInteger a, BigInteger b) {
       BigInteger tich = a.multiply(b);
       return Result.builder().kq(tich).build();
    }
}
