package com.todo.task.student.dohungcontrol.repository;

import com.todo.task.student.dohungcontrol.model.database.Employee;
import com.todo.task.student.dohungcontrol.model.database.EmployeeRole;
import com.todo.task.student.dohungcontrol.repository.database.EmployeeDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class EmployeeRespository {
    @Autowired
    EmployeeDatabase employeeDatabase;
    public List<Employee> findAllEmployee() { return employeeDatabase.findAll(); }

    public List<Employee> findAllEmployeeQuery() { return employeeDatabase.findAllByNativeQuery();}

    public List<Employee> findWithPage(int pageIndex, int pageSize) {
        Pageable pageable = PageRequest.of(pageIndex,pageSize);
        return employeeDatabase.findWithPage(pageable);
    }
    public List<EmployeeRole> findEmployeeInfo(int pageIndex, int pageSize) {
        Pageable pageable = PageRequest.of(pageIndex,pageSize);
        return employeeDatabase.findEmployeeInfo(pageable);
    }

    public boolean saveEmployee(Employee employee) {
        Employee employeeSave = employeeDatabase.save(employee);
        if (employeeSave != null) {
            return true;
        }
        return false;
    }
}
