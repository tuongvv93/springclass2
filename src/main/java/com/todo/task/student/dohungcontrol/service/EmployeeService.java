package com.todo.task.student.dohungcontrol.service;

import com.todo.task.student.dohungcontrol.model.database.Employee;
import com.todo.task.student.dohungcontrol.model.database.EmployeeRole;
import com.todo.task.student.dohungcontrol.repository.EmployeeRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRespository employeeRespository;
    public List<Employee> findAll(){ return employeeRespository.findAllEmployee();}

    public List<Employee> findAllEmployee()
    { return employeeRespository.findAllEmployeeQuery(); }

    public List<Employee> getWithPage(Integer page, Integer pageSize) { return employeeRespository.findWithPage(page,pageSize);}

    public boolean saveEmployee(Employee employee) {return employeeRespository.saveEmployee(employee);}

    public List<EmployeeRole> getInfo(Integer page,Integer pageSize) { return employeeRespository.findEmployeeInfo(page,pageSize);}
}
