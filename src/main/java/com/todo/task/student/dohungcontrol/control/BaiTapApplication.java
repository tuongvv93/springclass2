package com.todo.task.student.dohungcontrol.control;

import com.todo.task.student.dohungcontrol.model.ArrayFindMax;
import com.todo.task.student.dohungcontrol.model.BigNumber;
import com.todo.task.student.dohungcontrol.model.NumberMax;
import com.todo.task.student.dohungcontrol.model.Result;
import com.todo.task.student.dohungcontrol.model.base.BaseResponse;
import com.todo.task.student.dohungcontrol.model.database.Employee;
import com.todo.task.student.dohungcontrol.model.database.Students;
import com.todo.task.student.dohungcontrol.repository.EmployeeRespository;
import com.todo.task.student.dohungcontrol.repository.StudentRespository;
import com.todo.task.student.dohungcontrol.service.BigNumBerService;
import com.todo.task.teach.model.database.Staff;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("dohung")
public class BaiTapApplication {
    final
    BigNumBerService bigNumBerService;
    final StudentRespository studentRespository;
    final EmployeeRespository employeeRespository;

    public BaiTapApplication(BigNumBerService bigNumBerService,StudentRespository studentRespository,EmployeeRespository employeeRespository) {
        this.bigNumBerService = bigNumBerService;
        this.studentRespository = studentRespository;
        this.employeeRespository = employeeRespository;
    }

    @GetMapping("/bai1")
    public int bai1(@RequestParam int min, int max) {
        Random rd = new Random();
        return rd.nextInt((max - min) + 1) + min;
    }

    @PostMapping("/find-max")
    NumberMax findMax(@RequestBody ArrayFindMax arrayFindMax) {
        int max1 = 0;
        int nearMax1 = 0;
        for (int i = 0; i < arrayFindMax.getArr().length; i++) {
            if (arrayFindMax.getArr()[i] > max1) {
                max1 = arrayFindMax.getArr()[i];
            }
        }
        for (int j = 0; j < arrayFindMax.getArr().length; j++) {
            if (arrayFindMax.getArr()[j] > nearMax1 && arrayFindMax.getArr()[j] != max1) {
                nearMax1 = arrayFindMax.getArr()[j];
            }
        }
        return NumberMax.builder().max(max1).nearMax(nearMax1).build();
    }
    @GetMapping("/GetBigNumber")
    public String getBigNumber(@RequestParam BigInteger a,@RequestParam BigInteger b){
        BigInteger tich = a.multiply(b);
        return String.valueOf(tich);
    }
    @PostMapping("/PostNumber")
    public ResponseEntity<BaseResponse<Result>> postNumber(@RequestBody BigNumber bigNumber) {
        Result result = bigNumBerService.findTich(bigNumber.getA(),bigNumber.getB());
        BaseResponse<Result> baseResponse = new BaseResponse<>();
        baseResponse.setCode(1);
        baseResponse.setData(result);
        baseResponse.setMessage("abc");
        return ResponseEntity.ok(baseResponse);
    }

    @GetMapping("/getAllStudent")
    public List<Students> getAllStudent() {
        return studentRespository.listAllStudent();
    }
    @GetMapping("/getAllEmployee")
    public List<Employee> getAllEmployee() {
        return employeeRespository.findAllEmployee();
    }
}
